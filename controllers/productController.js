const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newProduct" and instantiates a new "product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.productprice
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// product creation successful
			if (error) {
				return false;
			// product creation failed
			} else {
				return true;
			};
		});
	// User is not an admin
	};

	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) =>{
		return {value};
	});
};
